**GIT**

- [1] Git is a distributed version-control system for tracking changes in source code during software development.

- [2] Git lets you easily keep track of every revision you and your team make during the development of your software.

- [3] It is designed for coordinating work among programmers, but it can be used to track changes in any set of files.

*Repository*
Often called as a repo. A repository is the collection of files and folders (code files) that you’re using git to track. It’s the big box you and your team throw your code into.

*Commit*
Think of this as saving your work. When you commit to a repository, it’s like you’re taking picture/snapshot of the files as they exist at that moment. The commit will only exist on your local machine until it is pushed to a remote repository.

*Push*
Pushing is essentially syncing your commits to GitLab.

*Branch*
You can think of your git repo as a tree. The trunk of the tree, the main software, is called the Master Branch. The branches of that tree are, well, called branches. These are separate instances of the code that is different from the main codebase.

*Merge*
When a branch is free of bugs (as far as you can tell, at least), and ready to become part of the primary codebase, it will get merged into the master branch. Merging is just what it sounds like: integrating two branches together.

*Clone*
Cloning a repo is pretty much exactly what it sounds like. It takes the entire online repository and makes an exact copy of it on your local machine.

*Fork*
Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.

