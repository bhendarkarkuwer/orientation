**GITLAB**

- [1] GitLab is a Git-based repository manager and a powerful complete application for software development.

- [2] GitLab is a complete DevOps platform, delivered as a single application. This makes GitLab unique and makes Concurrent DevOps possible, unlocking your organization from the constraints of a pieced together toolchain.

- [3] With an "user-and-newbie-friendly" interface, GitLab allows us to work effectively, both from the command line and from the UI itself.

- [4] It's not only useful for developers, but can also be integrated across our entire team to bring everyone into a single and unique platform.


**Features of Gitlab**


- Free service.

- Open source code repository.

- Bug tracking mechanism.

- Sharing of code with different people.


